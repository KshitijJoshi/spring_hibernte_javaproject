package spring_hibernate_project.com.spiders.dservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring_hibernate_project.com.spiders.dao.UserDao;
import spring_hibernate_project.com.spiders.dto.LoginDto;
import spring_hibernate_project.com.spiders.dto.UserDto;

@Service
public class LoginService {

	@Autowired
	private UserDao userDao;
	public UserDto login(LoginDto login) {
		String logiString = login.getLogin();
	/*	
		System.out.println("-----------");
		System.out.println(login);
		System.out.println(login.getLogin());
		System.out.println("-----------");*/

		// @ . is there in loginString
		if (logiString.contains("@") && logiString.contains(".")) {
			return userDao.findByEmail(logiString);
		}
		
		try {
			
		return 	userDao.findByMobileNo(Integer.parseInt(logiString)+"");
			
		} catch (Exception e) {
			return userDao.findByName(logiString);
		}
		
		
		
	}

}
