package spring_hibernate_project.com.spiders.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import spring_hibernate_project.com.spiders.dto.UserDto;

@Repository
public class UserDao {

	@Autowired
	SessionFactory factory;

	public long saveUser(UserDto user) {
		Session session = null;
		long id = 0;
		try {
			session = factory.openSession();
			session.beginTransaction();
			id = (long) session.save(user);
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}

		return id;
	}
	
	
	public void  updateUser(UserDto user) {
		Session session = null;
		long id = 0;
		try {
			session = factory.openSession();
			session.beginTransaction();
		 session.saveOrUpdate(user);
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		}
	}

	public UserDto findByEmail(String email) {

		try (Session session = factory.openSession();) {

			Query query = session.createQuery("select user from UserDto user " + "where user.eMail = :email");
			query.setParameter("email", email);
			return (UserDto) query.uniqueResult();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public UserDto findByName(String name) {

		try (Session session = factory.openSession();) {

			Query query = session.createQuery("select user from UserDto user " + "where user.name = :name");
			query.setParameter("name", name);
			return (UserDto) query.uniqueResult();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public UserDto findByMobileNo(String mobileNo) {

		try (Session session = factory.openSession();) {

			Query query = session.createQuery("select user from UserDto user " +
			"where user.mobileNo = :mobileNo");
			query.setParameter("mobileNo", mobileNo);
			return (UserDto) query.uniqueResult();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
	public UserDto findById(long iD) {

		try (Session session = factory.openSession();) {
			 UserDto userFromDataBase = session.get(UserDto.class, new Long(iD));
			return userFromDataBase;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	

}
