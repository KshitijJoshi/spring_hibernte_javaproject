package spring_hibernate_project.com.spiders.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import spring_hibernate_project.com.spiders.dservice.UserService;
import spring_hibernate_project.com.spiders.dto.UserDto;

@Controller
@RequestMapping("*")
public class UserController {

	@Autowired
	private UserService service;
	
	@PostMapping("/saveUser.do")
	ModelAndView saveUser(UserDto user){
		
		System.out.println("controller has been entered");
		long id = service.saveUser(user);
		System.out.println(id);
		if(id > 0 ){
		return new ModelAndView("success.jsp","user" , user);
		}
		return new ModelAndView("failure.jsp","user" , user);
	}
	
	
}
