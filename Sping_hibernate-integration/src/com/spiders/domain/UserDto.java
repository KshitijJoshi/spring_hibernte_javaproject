package com.spiders.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name ="employee")
public class UserDto  implements Serializable{

	@Id
	@GeneratedValue
	private long id ;
	
	@Column(name = "employee_name")
	private String name ;
	
	@Column(name = "employee_department")
	private String department;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "employee_joing_date")
	private Date dateOfJoining;
	
	@Column(name = "employee_salary")
	private long salary;
	
	@Column(name = "employee_password")
	private String password;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public Date getDateOfJoing() {
		return dateOfJoining;
	}

	public void setDateOfJoing(Date dateOfJoing) {
		this.dateOfJoining = dateOfJoing;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "EmployeeDto [id=" + id + ", name=" + name + ", department=" + department + ", dateOfJoing="
				+ dateOfJoining + ", salary=" + salary + ", password=" + password + "]";
	}

	public UserDto(long id, String name, String department, Date dateOfJoing, long salary, String password) {
		super();
		this.id = id;
		this.name = name;
		this.department = department;
		this.dateOfJoining = dateOfJoing;
		this.salary = salary;
		this.password = password;
	}

	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
}
