package com.spiders.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spiders.domain.UserDto;
import com.spiders.service.UserSerivice;

@Controller
@RequestMapping("*")
public class UserController {

	@Autowired 
	private  UserSerivice employeeSerivice;
	
//	@RequestMapping(value ="/admin/add/user.do" , method = RequestMethod.POST)
	@PostMapping("addUser.do")
	ModelAndView saveEmp( UserDto employeeDto) {
			
		int id = employeeSerivice.save(employeeDto);
		if(id > 0) {
			
			return new ModelAndView("dashboard.jsp","userName",employeeDto.getName());
		}
		return new ModelAndView("error.jsp");
		
	}
	
}
