package com.spiders.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.spiders.domian.util.loginDto;
import com.spiders.service.UserSerivice;

@Controller
@RequestMapping("*")
public class LoginController {
	
	@Autowired UserSerivice userSerivice;
	

	@PostMapping("loginUser.do")
	ModelAndView login(@ModelAttribute loginDto login ) {
		
		// login logic will come here
	
		return new ModelAndView("");
	}
}
