package com.spiders.repository;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spiders.domain.UserDto;

@Repository
public class UserDao {
	
	@Autowired
	private SessionFactory factory;

	public int addEmployee(UserDto user) {
		Session session = factory.openSession();
		session.beginTransaction();
		Long save = (Long) session.save(user);
		session.getTransaction().commit();
	
		session.close();
		return save.intValue();
	
	}
	
	
	public UserDto findOne(String login) {
	
		Session session = factory.openSession();
		Query query = session.createQuery("from EmployeeDto where name=:name  ");
		query.setParameter("name ", login);
		return (UserDto)query.uniqueResult();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
