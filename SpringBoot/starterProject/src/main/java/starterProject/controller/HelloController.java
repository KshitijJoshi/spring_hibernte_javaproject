package starterProject.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping(value="helloStudents")
	public String sayHello(){
		return "hello Jspiders Student";
	}
	
	
	@RequestMapping(value="helloAllStudents")
	public List< String> sayAllHello(){
		
		//String [] allStudents = new String
		List<String> listOfStds = new ArrayList<String>();
		listOfStds.add("Hello rajat");
		listOfStds.add("Hello Akshay");
		listOfStds.add("Hello Kshitij");
		listOfStds.add("Hello Kavya");
		listOfStds.add("Hello monika");
		
		return listOfStds;
	}
	

}
