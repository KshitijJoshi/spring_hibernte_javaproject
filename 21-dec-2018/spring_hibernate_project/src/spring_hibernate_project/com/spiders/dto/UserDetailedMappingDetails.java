package spring_hibernate_project.com.spiders.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_detailed_mapping")
public class UserDetailedMappingDetails implements Serializable{

	@Id
	@GeneratedValue
	private long id;
	
	@OneToOne
	private UserDto user;

	@Column(name = "no_of_family_members")
	private int noOfFamilyMembers;
	
	@Column(name = "address")
	private String Address;
	
	@Column(name = "salary")
	private double salary;
	
	public UserDetailedMappingDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Column(name = "date_of_leaving")
	private Date dateOfLeaving;

	@Column(name = "fathers_name")
	private String fathersName;

	@Column(name = "mother_name")
	private String mothersNAme;

	@Override
	public String toString() {
		return "UserDetailedMappingDetails [id=" + id + ", noOfFamilyMembers=" + noOfFamilyMembers + ", Address="
				+ Address + ", salary=" + salary + ", dateOfLeaving=" + dateOfLeaving + ", fathersName=" + fathersName
				+ ", mothersNAme=" + mothersNAme + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNoOfFamilyMembers() {
		return noOfFamilyMembers;
	}

	public void setNoOfFamilyMembers(int noOfFamilyMembers) {
		this.noOfFamilyMembers = noOfFamilyMembers;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Date getDateOfLeaving() {
		return dateOfLeaving;
	}

	public void setDateOfLeaving(Date dateOfLeaving) {
		this.dateOfLeaving = dateOfLeaving;
	}

	public String getFathersName() {
		return fathersName;
	}

	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}

	public String getMothersNAme() {
		return mothersNAme;
	}

	public void setMothersNAme(String mothersNAme) {
		this.mothersNAme = mothersNAme;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

}
