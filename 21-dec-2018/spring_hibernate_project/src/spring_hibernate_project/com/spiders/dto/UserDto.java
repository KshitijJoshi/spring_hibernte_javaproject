package spring_hibernate_project.com.spiders.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_user")
public class UserDto implements Serializable {
	
	@Id
	@GeneratedValue
	private long id;
	
	@OneToOne(mappedBy = "user" , cascade= CascadeType.ALL,fetch = FetchType.LAZY)
	private UserDetailedMappingDetails userMapping;
	
	@Column(name = "user_name", unique=true)
	private String name ;
	@Column(name = "user_email" , unique=true)
	private String eMail;
	@Column(name = "user_password")
	private String password;
	@Column(name = "user_role")
	private String role;
	@Column(name = "user_doj")
	private Date dateOfJoining;
	@Column(name = "user_mobile_num", unique=true)
	private String mobileNo;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Date getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", eMail=" + eMail + ", password=" + password + ", role=" + role
				+ ", dateOfJoining=" + dateOfJoining + ", mobileNo=" + mobileNo + "]";
	}
	public UserDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserDto(long id, String name, String eMail, String password, String role, Date dateOfJoining, String mobileNo) {
		super();
		this.id = id;
		this.name = name;
		this.eMail = eMail;
		this.password = password;
		this.role = role;
		this.dateOfJoining = dateOfJoining;
		this.mobileNo = mobileNo;
	}
	public UserDetailedMappingDetails getUserMapping() {
		return userMapping;
	}
	public void setUserMapping(UserDetailedMappingDetails userMapping) {
		this.userMapping = userMapping;
	}
	
	

}
