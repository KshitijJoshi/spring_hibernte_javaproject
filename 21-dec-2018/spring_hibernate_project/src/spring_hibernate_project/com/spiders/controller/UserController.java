package spring_hibernate_project.com.spiders.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import spring_hibernate_project.com.spiders.dservice.UserService;
import spring_hibernate_project.com.spiders.dto.UserDto;

@Controller
@RequestMapping("*")
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping("/saveUser.do")
	ModelAndView saveUser(UserDto user) {

		System.out.println("controller has been entered");
		long id = service.saveUser(user);
		System.out.println(id);
		if (id > 0) {
			return new ModelAndView("success.jsp", "user", user);
		}
		return new ModelAndView("failure.jsp", "user", user);
	}

	@PostMapping("/updateUser.do")
	ModelAndView updateUser(UserDto userFormHtml, HttpServletRequest request) {

		// http session object
		UserDto user = (UserDto) request.getSession().getAttribute("user");
		int id = (int) request.getSession().getAttribute("userId");

		// give a object from database
		UserDto userFromDataBase = service.findById(user.getId());
		UserDto userFromDataBase2 = service.findById(id);

		userFromDataBase.seteMail(userFormHtml.geteMail() != "" ? 
				userFormHtml.geteMail()  : "" );
		userFromDataBase.setMobileNo(userFormHtml.getMobileNo());
		userFromDataBase.setName(userFormHtml.getName());

		service.updateUSer(userFromDataBase);
		return new ModelAndView("failure.jsp");
	}

	@GetMapping("/deleteUser.do")
	ModelAndView deleteUser(@RequestParam(name = "pwd1") String pwd1, @RequestParam(name = "pwd2") String pwd2,
			HttpServletRequest request) {

		// this an object from session
		Object object = request.getSession().getAttribute("user");
		UserDto user = (UserDto) object;

		String pwd3 = user.getPassword();

		if (service.encode(pwd1).equals(pwd3) && service.encode(pwd2).equals(pwd3)) {

			service.deleteUSer(user.getId());
			System.out.println("user delated");
			return new ModelAndView("success.jsp");
		}

		return new ModelAndView("deleteUser.jsp", "msg", "credential wrong");
	}

}
