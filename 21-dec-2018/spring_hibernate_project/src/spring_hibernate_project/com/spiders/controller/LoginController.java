package spring_hibernate_project.com.spiders.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import spring_hibernate_project.com.spiders.dservice.LoginService;
import spring_hibernate_project.com.spiders.dservice.UserService;
import spring_hibernate_project.com.spiders.dto.LoginDto;
import spring_hibernate_project.com.spiders.dto.UserDto;

@Controller
@RequestMapping("*")
public class LoginController {
	
	final String USER_ROLE = "USER";

	@Autowired
	LoginService loginService;
	
	@Autowired
	UserService userService;
	
	@PostMapping("/loginUser.do")
	ModelAndView login(LoginDto loginDto,HttpServletRequest request ,ModelMap map){
		
		UserDto user = loginService.login(loginDto);
		
		if(user != null &&user.getPassword().equals(userService.encode(loginDto.getPassword()
				)) ){
			
			if(user.getRole().equalsIgnoreCase(USER_ROLE)){
				request.getSession().setAttribute("user", user);
				request.getSession().setAttribute("userId", user.getId());
				request.setAttribute("loginUser", user);
				//map.put("key", "value");
				return new ModelAndView("userDashboard.jsp","user",user);	
			}
			else{
				request.setAttribute("loginUser", user);
				return new ModelAndView("adminDashboard.jsp","user",user);
			}
			
		}
		
		return new ModelAndView("login.jsp","msg" ,"Worng credentials");
	}
	
	
	
}
