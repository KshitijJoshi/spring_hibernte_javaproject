package spring_hibernate_project.com.spiders.dservice;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring_hibernate_project.com.spiders.dao.UserDao;
import spring_hibernate_project.com.spiders.dto.UserDto;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	public long saveUser(UserDto user) {
		Date date = new Date();
		user.setDateOfJoining(date);

		String encodededPassword = encode(user.getPassword());
		user.setPassword(encodededPassword);

		return userDao.saveUser(user);
	}

	public UserDto findById(long id) {

		return userDao.findById(id);
	}

	public void updateUSer(UserDto dto) {
		userDao.updateUser(dto);
	}

	public void deleteUSer(long id) {
		userDao.deleteUser(id);
	}

	public String encode(String password) {
		String encoded = "!@#$%^&*()";
		int ch = 'a';

		String output = "";
		for (int i = 0; i < password.length(); i++) {
			int charAt = password.charAt(i);

			String out = "";
			while (charAt > 0) {

				char c = encoded.charAt(charAt % 10);
				out = c + out;
				charAt = charAt / 10;
			}

			output += out;
		}

		return output;
	}

}
